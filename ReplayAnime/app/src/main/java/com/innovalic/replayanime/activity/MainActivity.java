package com.innovalic.replayanime.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.ads.AdSize;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.innovalic.replayanime.R;
import com.innovalic.replayanime.fragment.Fragment01;
import com.innovalic.replayanime.fragment.Fragment02;
import com.innovalic.replayanime.fragment.Fragment03;
import com.innovalic.replayanime.fragment.Fragment04;
import com.innovalic.replayanime.fragment.Fragment05;
import com.innovalic.replayanime.fragment.Fragment06;
import com.innovalic.replayanime.fragment.Fragment07;
import com.innovalic.replayanime.fragment.Fragment08;
import com.innovalic.replayanime.fragment.Fragment2016_1;
import com.innovalic.replayanime.fragment.Fragment2016_2;
import com.innovalic.replayanime.fragment.Fragment2016_3;

public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private final String TAG = " MainActivity - ";

    private InterstitialAd interstitialAd;

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FORTH = 3;
    public final static int FRAGMENT_FIFTH = 4;
    public final static int FRAGMENT_SIXTH = 5;
    public final static int FRAGMENT_SEVENTH = 6;
    public final static int FRAGMENT_EIGHTH = 7;
    public final static int FRAGMENT_NINETH = 8;
    public final static int FRAGMENT_TENTH = 9;
    public final static int FRAGMENT_ELEVENTH = 10;

    private TextView fragment01;
    private TextView fragment02;
    private TextView fragment03;
    private TextView fragment04;
    private TextView fragment05;
    private TextView fragment06;
    private TextView fragment07;
    private TextView fragment08;
    private TextView fragment09;
    private TextView fragment10;
    private TextView fragment11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        AdView mAdViewUpper = (AdView) findViewById(R.id.adView_upper);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewUpper.loadAd(adRequest);

        /*NativeExpressAdView admobNative = new NativeExpressAdView(this);
        admobNative.setAdSize(new AdSize(320, 250));
        admobNative.setAdUnitId("ca-app-pub-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        nativeContainer.addView(admobNative);
        admobNative.loadAd(adRequest);*/


        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/7344126459");
        interstitialAd.loadAd(adRequest);

        fragment01 = (TextView)findViewById(R.id.tv_fragment01);
        fragment02 = (TextView)findViewById(R.id.tv_fragment02);
        fragment03 = (TextView)findViewById(R.id.tv_fragment03);
        fragment04 = (TextView)findViewById(R.id.tv_fragment04);
        fragment05 = (TextView)findViewById(R.id.tv_fragment05);
        fragment06 = (TextView)findViewById(R.id.tv_fragment06);
        fragment07 = (TextView)findViewById(R.id.tv_fragment07);
        fragment08 = (TextView)findViewById(R.id.tv_fragment08);
        fragment09 = (TextView)findViewById(R.id.tv_fragment09);
        fragment10 = (TextView)findViewById(R.id.tv_fragment10);
        fragment11 = (TextView)findViewById(R.id.tv_fragment11);

        fragment01.setOnClickListener(this);
        fragment02.setOnClickListener(this);
        fragment03.setOnClickListener(this);
        fragment04.setOnClickListener(this);
        fragment05.setOnClickListener(this);
        fragment06.setOnClickListener(this);
        fragment07.setOnClickListener(this);
        fragment08.setOnClickListener(this);
        fragment09.setOnClickListener(this);
        fragment10.setOnClickListener(this);
        fragment11.setOnClickListener(this);

        mCurrentFragmentIndex = FRAGMENT_ONE;     // 첫 Fragment 를 초기화
        fragmentReplace(mCurrentFragmentIndex);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.tv_fragment01 :
                offColorTv();
                fragment01.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment02 :
                offColorTv();
                fragment02.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment03 :
                offColorTv();
                fragment03.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment04 :
                offColorTv();
                fragment04.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_FORTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment05 :
                offColorTv();
                fragment05.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_FIFTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment06 :
                offColorTv();
                fragment06.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_SIXTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment07 :
                offColorTv();
                fragment07.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_SEVENTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment08 :
                offColorTv();
                fragment08.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_EIGHTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment09 :
                offColorTv();
                fragment09.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_NINETH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment10 :
                offColorTv();
                fragment10.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_TENTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment11 :
                offColorTv();
                fragment11.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_ELEVENTH;
                fragmentReplace(mCurrentFragmentIndex);
                break;
        }
    }

    public void fragmentReplace(int reqNewFragmentIndex) {

        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        // replace fragment
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.ll_fragment, newFragment);
        // Commit the transaction
        transaction.commit();

    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;

        switch (idx) {
            case FRAGMENT_ONE:
                newFragment = new Fragment01();
                break;
            case FRAGMENT_TWO:
                newFragment = new Fragment02();
                break;
            case FRAGMENT_THREE:
                newFragment = new Fragment03();
                break;

            case FRAGMENT_FORTH:
                newFragment = new Fragment04();
                break;

            case FRAGMENT_FIFTH:
                newFragment = new Fragment05();
                break;

            case FRAGMENT_SIXTH:
                newFragment = new Fragment06();
                break;

            case FRAGMENT_SEVENTH:
                newFragment = new Fragment07();
                break;
            case FRAGMENT_EIGHTH:
                newFragment = new Fragment08();
                break;
            case FRAGMENT_NINETH:
                newFragment = new Fragment2016_1();
                break;
            case FRAGMENT_TENTH:
                newFragment = new Fragment2016_2();
                break;
            case FRAGMENT_ELEVENTH:
                newFragment = new Fragment2016_3();
                break;
            default:
                Log.d(TAG, "Unhandle case");
                break;
        }

        return newFragment;
    }

    public void offColorTv(){
        fragment01.setBackgroundResource(R.color.pink_a100);
        fragment02.setBackgroundResource(R.color.pink_a100);
        fragment03.setBackgroundResource(R.color.pink_a100);
        fragment04.setBackgroundResource(R.color.pink_a100);
        fragment05.setBackgroundResource(R.color.pink_a100);
        fragment06.setBackgroundResource(R.color.pink_a100);
        fragment07.setBackgroundResource(R.color.pink_a100);
        fragment08.setBackgroundResource(R.color.pink_a100);
        fragment09.setBackgroundResource(R.color.pink_a100);
        fragment10.setBackgroundResource(R.color.pink_a100);
        fragment11.setBackgroundResource(R.color.pink_a100);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(interstitialAd.isLoaded()){
            interstitialAd.show();
        }
    }

}