package com.innovalic.replayanime.model;

/**
 * Created by Administrator on 2016-07-02.
 */
public class ListViewItem {
    String imgUrl;
    String title;
    String episode;

    public ListViewItem(){

    }

    public ListViewItem(String imgUrl, String title, String episode) {
        this.imgUrl = imgUrl;
        this.title = title;
        this.episode = episode;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }
}
