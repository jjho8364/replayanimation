package com.innovalic.replayanime.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.innovalic.replayanime.R;
import com.innovalic.replayanime.activity.AniListActivity;
import com.innovalic.replayanime.adapter.GridViewAdapter;
import com.innovalic.replayanime.model.GridViewItem;
import com.innovalic.replayanime.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-02.
 */
public class Fragment2016_2 extends Fragment implements View.OnClickListener {
    private final String TAG = " Fragment10 - ";
    private ProgressDialog mProgressDialog;
    private ArrayList<GridViewItem> gridArr;
    private final String baseUrl = "http://moeni.net";
    private final String categoryUrl = "/2016-2/";
    private int pageNum = 1;
    private GridView gridView;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private Button searchBtn;
    private EditText eidtText;
    GetGridView asyncGridview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        String model = Build.MODEL.toLowerCase();
        TelephonyManager telephony = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telephony.getNetworkOperator();
        int portrait_width_pixel=Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        boolean phoneNumFlag = false;
        boolean operatorFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        //Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        //int contactsCnt = c.getCount();
        //if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        if(operator == null || operator.equals("")){ operatorFlag = true; }
        if ( (model.equals("sph-d720") || model.contains("nexus") || operatorFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment08, container, false);
            gridView = (GridView)view.findViewById(R.id.gridview08);
            tv_currentPage = (TextView)view.findViewById(R.id.fr08_currentpage);
            tv_lastPage = (TextView)view.findViewById(R.id.fr08_lastpage);
            preBtn = (Button)view.findViewById(R.id.fr08_prebtn);
            nextBtn = (Button)view.findViewById(R.id.fr08_nextbtn);
            searchBtn = (Button)view.findViewById(R.id.fr08_searchbtn);
            eidtText = (EditText)view.findViewById(R.id.fr08_edit);

            preBtn.setOnClickListener(this);
            nextBtn.setOnClickListener(this);
            searchBtn.setOnClickListener(this);

            asyncGridview = new GetGridView();//.execute();
            asyncGridview.execute();
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr08_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    //asyncGridview.execute();
                    new GetGridView().execute();
                }
                break;

            case R.id.fr08_nextbtn :
                if(pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    //asyncGridview.execute();
                    new GetGridView().execute();
                }
                break;
            case R.id.fr08_searchbtn :
                Log.d(TAG, "123");
                if(!eidtText.getText().toString().equals("")){
                    Log.d(TAG, "456");
                    GetGridSearchView getGridSearchView = new GetGridSearchView(eidtText.getText().toString());
                    getGridSearchView.execute();
                }


                break;
        }
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            gridArr = null;
            gridArr = new ArrayList<GridViewItem>();

            String addUrl = "";
            if(pageNum != 1){
                addUrl = "page/" + pageNum;
            }

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl + categoryUrl + addUrl).timeout(10000).get();

                Elements table = doc.select(".main");
                Elements trs = table.select("a .img");
                Elements navUpper = table.select(".nav-links");
                Elements nav =  navUpper.select("a");

                Elements titles = doc.select("#primary");
                Elements as1 = titles.select("a");

                if(nav.size() == 0){
                    Log.d(TAG, "nav.size() : " + nav.size());
                    //tempLastPage = tv_lastPage.getText().toString();
                } else {
                    if(nav.get(nav.size()-1).text().equals("다음 쪽")){
                        tempLastPage = nav.get(nav.size()-2).text().split(" ")[1];
                    } else {
                        tempLastPage = nav.get(nav.size()-1).text().split(" ")[1];
                    }
                }

                //tempLastPage = as.get(as.size() - 2).text();
                //Log.d(TAG, as.get(as.size() - 2).text());

                for(int i=0 ; i<trs.size() ; i++){
                    String imgUrl = trs.get(i).select("img").attr("src");
                    String title = trs.get(i).select("img").attr("alt");
                    String totalEpisode = trs.get(i).select("#img_text div:nth-child(1)").text();
                    String updateDT = trs.get(i).select("#img_text div:nth-child(2)").text();
                    String listUrl = as1.get(i).attr("href");
                    String totalPageNum = "";

                    gridArr.add(new GridViewItem(imgUrl, title, totalEpisode, updateDT, listUrl));
                    //Log.d(TAG, imgUrl);
                    //Log.d(TAG, title);
                    //Log.d(TAG, totalEpisode);
                    //Log.d(TAG, updateDT);
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tv_currentPage.setText(pageNum+"");
            if(pageNum >= Integer.parseInt(tempLastPage)){
                tv_lastPage.setText(pageNum+"");
            } else {
                tv_lastPage.setText(tempLastPage);
            }

            // adapter에 적용
            gridView.setAdapter(new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem));

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (NetworkUtil.getConnectivity(getActivity())) {
                        String listUrl = gridArr.get(position).getListUrl();
                        //Log.d(TAG, listUrl);
                        Intent intent = new Intent(getActivity(), AniListActivity.class);
                        intent.putExtra("listUrl", listUrl);
                        intent.putExtra("imgUrl", gridArr.get(position).getImgUrl());
                        Log.d(TAG, listUrl);
                        intent.putExtra("categoryUrl", "월");
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        asyncGridview.cancel(true);

    }

    public class GetGridSearchView extends AsyncTask<Void, Void, Void> {

        String searchUrl;
        public GetGridSearchView(String item){
            searchUrl = item;
        }

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            gridArr = null;
            gridArr = new ArrayList<GridViewItem>();

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl + "/?s=" + searchUrl).timeout(10000).get();

                Elements main = doc.select("#content");
                Elements divs = main.select("a");

                Log.d(TAG, "divs.size() : " + divs.size());
                for(int i=0 ; i<divs.size() ; i++){
                    String imgUrl = divs.get(i).select("img").attr("src");
                    String listUrl = divs.get(i).attr("href");
                    String title = divs.get(i).select("img").attr("alt");
                    String totalEpisode = divs.get(i).select(".img_text:nth-child(1)").text();
                    String updateDT = divs.get(i).select(".img_text:nth-child(2)").text();

                    Log.d(TAG, "listUrl : " + listUrl);

                    gridArr.add(new GridViewItem(imgUrl, title, totalEpisode, updateDT, listUrl));
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tv_currentPage.setText(pageNum+"");
            tv_lastPage.setText("1");

            // adapter에 적용
            gridView.setAdapter(new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem));

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (NetworkUtil.getConnectivity(getActivity())) {
                        String listUrl = gridArr.get(position).getListUrl();
                        //Log.d(TAG, listUrl);
                        Intent intent = new Intent(getActivity(), AniListActivity.class);
                        intent.putExtra("listUrl", listUrl);
                        intent.putExtra("imgUrl", gridArr.get(position).getImgUrl());
                        Log.d(TAG, listUrl);
                        intent.putExtra("categoryUrl", "월");
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "check your network connection status", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mProgressDialog.dismiss();
        }
    }

}
