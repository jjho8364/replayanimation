package com.innovalic.replayanime.model;

/**
 * Created by Administrator on 2016-06-30.
 */
public class GridViewItem {

    String imgUrl;
    String title;
    String totalEpisode;
    String updateDt;
    String listUrl;

    public GridViewItem(){

    }

    public GridViewItem(String imgUrl, String title, String totalEpisode, String updateDt, String listUrl) {
        this.imgUrl = imgUrl;
        this.title = title;
        this.totalEpisode = totalEpisode;
        this.updateDt = updateDt;
        this.listUrl = listUrl;
    }

    public String getListUrl() {
        return listUrl;
    }

    public void setListUrl(String listUrl) {
        this.listUrl = listUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotalEpisode() {
        return totalEpisode;
    }

    public void setTotalEpisode(String totalEpisode) {
        this.totalEpisode = totalEpisode;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }
}
